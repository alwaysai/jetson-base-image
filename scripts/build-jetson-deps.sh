#!/bin/bash
set -e
set -x

apt-get update && apt-get install -y git ccache protobuf-compiler libprotoc-dev

ln -s ccache /usr/local/bin/gcc
ln -s ccache /usr/local/bin/g++
ln -s ccache /usr/local/bin/cc
ln -s ccache /usr/local/bin/c++

cd /

# Build TensorRT

export TRT_OSSPATH=/TensorRT

git clone https://github.com/NVIDIA/TensorRT.git

cd $TRT_OSSPATH

git submodule update --init --recursive

export EXT_PATH=~/external

mkdir -p $EXT_PATH && cd $EXT_PATH

git clone https://github.com/pybind/pybind11.git

mkdir -p $EXT_PATH/python$PYTHON_DVERSION/include

cp -r /usr/local/include/python${PYTHON_DVERSION}m/* $EXT_PATH/python$PYTHON_DVERSION/include/

cd $TRT_OSSPATH/python

python3 -m pip install wheel

PYTHON_MAJOR_VERSION=${PYTHON_DVERSION:0:1} PYTHON_MINOR_VERSION=${PYTHON_DVERSION:2} TARGET_ARCHITECTURE=aarch64 ./build.sh

cp $TRT_OSSPATH/python/build/dist/* /artifacts

cd /

git clone https://github.com/onnx/onnx.git

cd onnx

git submodule update --init --recursive

python3 setup.py bdist_wheel

cp dist/*.whl /artifacts

cd /
