# Jetson Base Image

Use this repo to build the base image for the jetson platform.

## Usage

The image needs to be built on a jetson device or a Linux desktop with cross-compilation enabled.

```
To build the docker image with default configs, run the build script located at the root of the directory.
$ ./build.sh

To specify the L4T version, use --l4t-ver. The default is r32.4.4.
./build.sh --l4t-ver r32.4.4

To specify the Python version, use --python-ver. The default is 3.7.9.
./build.sh --python-ver 3.7.9
```
