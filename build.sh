#!/usr/bin/env bash

set -x
set -e

POSITIONAL=()
while [[ $# -gt 0 ]]; do
  key="$1"

  case $key in
    --l4t-ver)
      L4T_VERSION="$2"
      shift # past argument
      shift # past value
      ;;
    --python-ver)
      PYTHON_VERSION="$2"
      shift # past argument
      shift # past value
      ;;
    *)    # unknown option
      POSITIONAL+=("$1") # save it in an array for later
      shift # past argument
      ;;
  esac
done

set -- "${POSITIONAL[@]}" # restore positional parameters

if [[ -z "$L4T_VERSION" ]]; then
  L4T_VERSION="r32.4.4"
  echo "l4t version not specified, building ${L4T_VERSION}. Use --l4t-ver to specify a version."
fi

if [[ -z "$PYTHON_VERSION" ]]; then
  PYTHON_VERSION="3.7.9"
  echo "Python version not specified, building ${PYTHON_VERSION}. Use --python-ver to specify a version."
fi

PYTHON_DVERSION=${PYTHON_VERSION%.*}

docker build -t l4t-base-${L4T_VERSION}-py${PYTHON_DVERSION}-tmp \
	--build-arg L4T_VERSION=${L4T_VERSION} \
	--build-arg PYTHON_VERSION=${PYTHON_VERSION} \
	--build-arg PYTHON_DVERSION=${PYTHON_DVERSION} \
	-f docker/Dockerfile.initial \
	.

mkdir -p artifacts

docker run --rm \
	-i \
	-v ~/.ccache:/root/.ccache \
	-v `pwd`/artifacts:/artifacts \
	-e PYTHON_DVERSION=$PYTHON_DVERSION \
	--runtime=nvidia \
	l4t-base-${L4T_VERSION}-py${PYTHON_DVERSION}-tmp:latest \
	/bin/bash < scripts/build-jetson-deps.sh

docker build -t alwaysai/cuda:l4t-base-${L4T_VERSION}-py${PYTHON_DVERSION} \
	--build-arg L4T_VERSION=${L4T_VERSION} \
	--build-arg PYTHON_DVERSION=${PYTHON_DVERSION} \
	-f docker/Dockerfile.final \
	.
